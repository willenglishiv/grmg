# Memory Game

## Introduction

This was an application requested by GR as part of their job application process.  The requirements are stated below.

## Requirements

Memory Game

Using your choice of tooling or framework, develop a web based memory game.

Game Rules

* At the start of the game, the user is presented with a grid of 24 facedown cards.
* Each card looks identical face down, but has a face-up value that is matched by only one other card on the table.
* When the user clicks a card, it flips over revealing its value.
* When the user clicks the next card, its value is revealed and then compared against the other face up card. If they are equal, both cards disappear. If they are different, they flip back down.
* The game is continued until there are no cards left.

## How to Install

The app was built on the NodeJS platform, utilizing ExpressJS, UnderscoreJS and Bootstrap for a front-end framework, specifically from HTML5 Boilerplate

Installation instructions are as follows

Copy this directory or pull from the git repository into an empty directory.

Make sure that node and npm are installed on the host machine.

Run the command `npm install` in the root directory.  This will install everything related to the application.

Run `npm start` in the same directory, and point your browser to `http://localhost:3000` to view the application.

## How to play the game

The game will start with 24 cards on the screen at desktop resolution.  Select one card by clicking on the red box and it will flip over revealing a numerical value from 1-12.  Select a second red box, and if the numbers match, they will disappear from the game board.  Keep selecting matches until the entire board is empty, upon which you will see a 'You Win!' message on the screen.

## People

Developed and maintained by Will English IV