 'use strict';
 // Closure to prevent namespace issues
 (function($){

  // this problem involves retaining some level of 'state', which could be improved
  // in a later version to use REDUX, but this is a simple example.
  var CLICK_LOCK = false;
  var MATCHES = 0;
 	var BOARD = [];

 	var MEMORY = {

 		init: function() {

 			$(".card")
 				.flip({ trigger: 'manual' }) // initializing flip library
 				.on('click',MEMORY.cardClick) // click handler function
        .on('flip:done',MEMORY.checkBoardState); // want to check state after flip

      // trying to fix FOUC, if more time could do otherwise.
      $(".card .back")
        .css("border-color","#000")
        .css("background-color","#333");

 		},
 		cardClick: function(e) {
 			e.preventDefault();
      if (!CLICK_LOCK) {

        // immediately prevent user from clicking another card
        MEMORY.lockCards(); 
        var $this = $(this);
        var flipstate = $this.data("flip-model");
        // only condition we need to check is if we need to flip back down the 1st card
        if ( 1 === BOARD.length && flipstate.isFlipped ) {
          BOARD.pop();
        } else {
          BOARD.push( $this );
        }

        $this.flip('toggle');

      }

 		},
    unlockCards: function() {
      CLICK_LOCK = false;
    },
    lockCards: function() {
      CLICK_LOCK = true;
    },
    checkBoardState: function() {
      // does work if we have two cards flipped.
      if ( 2 === BOARD.length ) {

        // do comparison
        var $left = BOARD.pop();
        var $right = BOARD.pop();

        if ( $left.data('cardvalue') === $right.data('cardvalue') ) {
          // cards match, remove card and click handler so they can't click an empty card
          $left
            .add($right)
            .delay( 800 )
            .fadeOut(function(){
              MEMORY.unlockCards();
              MATCHES++;
              if ( 24 === MATCHES ) {
                MEMORY.finishTheGame();
              }

            })
            .off('click',MEMORY.cardClick);

        } else {
          setTimeout(function(){
            // give user some time (1 second) to view their choices before flipping back down
            $left.add($right).flip('toggle');
          },1000);
        }

      } else {
        // Otherwise unlock the cards and let the user pick another one.
        MEMORY.unlockCards();

      }

    },
    finishTheGame: function() {
      var $canvas = $("#main .row");
      $canvas.fadeOut(function(){
        $canvas.html('<h1>You win!</h1>').fadeIn();
      });
    }

 	};

 	$(document).ready(MEMORY.init);

 })(jQuery);
