var express = require('express');
var router = express.Router();
var _ = require('underscore');

/* GET home page. */
router.get('/', function(req, res, next) {
  // could be improved with a generator, but small sample size I just use 
  // underscore and shuffle
	let cards = _.shuffle([1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12]);

  res.render('index', { cards: cards });
});

module.exports = router;
